from argparse import Namespace, ArgumentParser
from typing import Dict, Any

from json2any_plugin.AbstractDataProvider import AbstractDataProvider


class ExampleDataProvider(AbstractDataProvider):

    def __init__(self):
        super().__init__()

    def load_data(self) -> Dict[str, Any]:
        return {
            "example": {
                "key": "value",
                "key2": "value2"
            }
        }

    def init(self):
        pass

    def update_arg_parser(self, parser: ArgumentParser) -> None:
        parser.add_argument('--example-data', help='Includes example data', action='store_true')

    def process_args(self, args: Namespace) -> bool:
        if args.example_data:
            return True
        return False
